import java.net.IDN;

public class Eiendom {
    private int kommunenummer;
    private String kommunenavn;
    private int gardsnummer;

    private int bruksnummer;

    private String bruksnavn;
    private double areal;
    private String navnPåEier;

    public Eiendom(int kommunenummer, String kommunenavn, int gardsnummer, int bruksnummer, String bruksnavn, double areal, String navnPåEier) {
        if (100 < kommunenummer && kommunenummer < 5055){
            this.kommunenummer = kommunenummer;
        } else {
            throw new IllegalArgumentException("Ugyldig kommunenummer. Det må være mellom 100 og 5055.");
        }
        this.kommunenavn = kommunenavn;
        this.gardsnummer = gardsnummer;
        this.bruksnummer = bruksnummer;
        this.bruksnavn = bruksnavn;
        this.areal = areal;
        this.navnPåEier = navnPåEier;
    }
    public String getKommunenavn() {
        return kommunenavn;
    }

    public int getKommunenummer() {
        return kommunenummer;
    }
    public int getgardsnummer() {
        return gardsnummer;
    }

    public int getBruksnummer() {
        return bruksnummer;
    }

    public String getBruksnavn() {
        return bruksnavn;
    }

    public double getAreal() {
        return areal;
    }

    public String getNavnPåEier() {
        return navnPåEier;
    }

    public void setKommunenummer(int kommunenummer) {
        this.kommunenummer = kommunenummer;
    }

    public void setKommunenavn(String kommunenavn) {
        this.kommunenavn = kommunenavn;
    }

    public void setBruksnavn(String bruksnavn) {
        this.bruksnavn = bruksnavn;
    }

    public void setAreal(double areal) {
        this.areal = areal;
    }

    public void setNavnPåEier(String navnPåEier) {
        this.navnPåEier = navnPåEier;
    }
    public String ID(){
        return kommunenummer + "-" + gardsnummer + "/" + bruksnummer;
    }

    @Override
    public String toString() {
        return  "Eiendom" +
                "\n  kommunenummer: " + kommunenummer +
                "\n  kommunenavn: " + kommunenavn +
                "\n  gårdsnumme: " + gardsnummer +
                "\n  bruksnummer: " + bruksnummer +
                "\n  bruksnavn: " + bruksnavn +
                "\n  areal: " + areal +
                "\n  navnPåEier: " + navnPåEier +
                "\n";

    }
}
