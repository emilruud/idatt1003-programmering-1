import java.util.ArrayList;
import java.util.Scanner;

public class brukerinteraksjone {
    private static final int ADD_PROPERTY = 1;
    private static final int LIST_ALL_PROPERTIES = 2;
    private static final int FIND_PROPERTY = 3;
    private static final int CALCULATE_AVERAGE_AREA = 4;
    private static final int EXIT = 9;

    /**
     * Presents the menu for the user, and awaits input from the user. The menu
     * choice selected by the user is being returned.
     *
     * @return the menu choice by the user as a positive number starting from 1.
     * If 0 is returned, the user has entered a wrong value
     */
    private int showMenu() {
        int menuChoice = 0;
        System.out.println("\n***** Property Register Application v0.1 *****\n");
        System.out.println("1. Add property");
        System.out.println("2. List all properties");
        System.out.println("3. Search property");
        System.out.println("4. Calculate average area");
        System.out.println("9. Quit");
        System.out.println("\nPlease enter a number between 1 and 9.\n");

        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            menuChoice = sc.nextInt();
        } else {
            System.out.println("You must enter a number, not text");
        }
        return menuChoice;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        EiendomRegister register = new EiendomRegister();
        Eiendom eiendom1 = new Eiendom(123, "Oslo", 456, 789, "Gård1", 100.0, "Eier1");
        Eiendom eiendom2 = new Eiendom(456, "Bergen", 789, 123, "Gård2", 150.0, "Eier2");
        Eiendom eiendom3 = new Eiendom(789, "Trondheim", 123, 456, "Gård3", 200.0, "Eier3");
        register.registrereEiendom(eiendom1);
        register.registrereEiendom(eiendom2);
        register.registrereEiendom(eiendom3);
        boolean finished = false;
        // The while-loop will run as long as the user has not selected
        // to quit the application
        while (!finished) {
            int menuChoice = this.showMenu();
            switch (menuChoice) {
                case ADD_PROPERTY:

                    System.out.println("Add property selected");
                    System.out.println("Skriv inn kommunenummer: ");
                    int kommunenummer = scanner.nextInt();
                    scanner.nextLine(); // Consume the newline character left in the buffer

                    System.out.println("skriv inn kommunenavn: ");
                    String kommunenavn = scanner.nextLine();

                    System.out.println("skriv inn gårdsnummer: ");
                    int gardnummer = scanner.nextInt();
                    scanner.nextLine(); // Consume the newline character left in the buffer

                    System.out.println("Skriv inn bruksnummer");
                    int bruksnummer = scanner.nextInt();
                    scanner.nextLine(); // Consume the newline character left in the buffer

                    System.out.println("Skriv inn bruksnavn: ");
                    String bruksnavn = scanner.nextLine();

                    System.out.println("Skriv inn areal: ");
                    double areal = scanner.nextDouble();
                    scanner.nextLine(); // Consume the newline character left in the buffer

                    System.out.println("Skriv inn navn på Eier");
                    String navnPåEier = scanner.nextLine();

                    Eiendom eiendom = new Eiendom(kommunenummer, kommunenavn, gardnummer, bruksnummer, bruksnavn, areal, navnPåEier);
                    register.registrereEiendom(eiendom);
                    break;
                case LIST_ALL_PROPERTIES:
                    // TODO: Fill in your code here....
                    listAllProperties(register);
                    break;
                case FIND_PROPERTY:
                    // TODO: Fill in your code here....
                    System.out.println("Søke etter eiendom basert på kommunenr, gnr og bnr");
                    System.out.println("Skriv inn kommunenummer: ");
                    int kommunenummer1 = scanner.nextInt();

                    System.out.println("skriv inn gårdsnummer: ");
                    int gardnummer1 = scanner.nextInt();

                    System.out.println("Skriv inn bruksnummer");
                    int bruksnummer1 = scanner.nextInt();

                    Eiendom eiendomSok = register.finnEiendom(kommunenummer1, gardnummer1, bruksnummer1);
                    System.out.println(eiendomSok);

                    break;
                case CALCULATE_AVERAGE_AREA:
                    // TODO: Fill in your code here....
                    System.out.println("Calculate average area selected");
                    double gjennomsnittsareal = register.gjennomsnittsareal();
                    System.out.println("Gjennomsnittsareal er: " + gjennomsnittsareal);
                    break;
                case EXIT:
                    System.out.println("Thank you for using the Properties app!\n");
                    finished = true;
                    break;
                default:
                    System.out.println("Unrecognized menu selected..");
                    break;
            }
        }
    }

    public void listAllProperties(EiendomRegister register){
        System.out.println("List all properties selected");
        ArrayList<Eiendom> eiendommer = register.getRegister();
        int i = 0;
        for (Eiendom eiendom4 : eiendommer){
            System.out.println( i + ": " + eiendom4.ID());
            i++;
        }
    }
}
