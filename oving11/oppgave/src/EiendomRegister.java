import java.util.ArrayList;

public class EiendomRegister {
    ArrayList<Eiendom> register;

    public EiendomRegister(){
        register = new ArrayList<>();
    }

    public ArrayList<Eiendom> getRegister(){
        return register;
    }
    public void registrereEiendom(Eiendom eiendom){
        register.add(eiendom);
    }
    public void sletteEiendom(Eiendom eiendom){
        register.remove(eiendom);
    }
    public int antallEiendommer(){
        return register.size();
    }
    public Eiendom finnEiendom(int kommunenummer, int gardsnummer, int bruksnummer){
        for (Eiendom eiendom : register) {
            if (eiendom.getKommunenummer() == kommunenummer && eiendom.getgardsnummer() == gardsnummer && eiendom.getBruksnummer() == bruksnummer) {
                return eiendom;
            }
        }
        return null;
    }

    public double gjennomsnittsareal(){
        double sumAreal = 0;
        for (Eiendom eiendom : register){
            sumAreal += eiendom.getAreal();
        }
        return sumAreal/register.size();
    }

    public ArrayList<Eiendom> eiendomerMedGardsnummer(int gardsnummer){
        ArrayList<Eiendom> resultat = new ArrayList<>();
        for (Eiendom eiendom : register){
            if (eiendom.getgardsnummer() == gardsnummer){
                resultat.add(eiendom);
            }
        }
        return resultat;
    }
}
