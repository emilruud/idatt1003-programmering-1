// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        brok brok1 = new brok(2, 4);
        brok brok2 = new brok(2, 2);
        brok1.addBrok(brok2);
        System.out.println("brøk1:");
        System.out.println(brok1.getTeller());
        System.out.println(brok1.getNevner());

        brok brok3 = new brok(7, 4);
        brok brok4 = new brok(2, 4);
        brok3.subtractBrok(brok4);
        System.out.println("brøk3:");
        System.out.println(brok3.getTeller());
        System.out.println(brok3.getNevner());

        brok brok5 = new brok(4, 5);
        brok brok6 = new brok(5, 9);
        brok5.multiplyBrok(brok6);
        System.out.println("brøk5:");
        System.out.println(brok5.getTeller());
        System.out.println(brok5.getNevner());

        brok brok7 = new brok(4, 5);
        brok brok8 = new brok(5, 9);
        brok7.divdeBrok(brok8);
        System.out.println("brøk7:");
        System.out.println(brok7.getTeller());
        System.out.println(brok7.getNevner());

        System.out.println("10 randome heltalltall og desimaltall:");
        MinRandom tall = new MinRandom();
        for (int i = 0; i < 10; i++){
            System.out.println(tall.nesteHeltall(10, 20));
            System.out.println(tall.nesteDesimalTall(10, 20));
        }
    }
}