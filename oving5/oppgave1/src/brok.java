public class brok {
    private int teller;
    private int nevner;

    public brok(int teller, int nevner) {
        if (nevner != 0) {
            this.teller = teller;
            this.nevner = nevner;
        } else {
            throw new IllegalArgumentException("Nevneren kan ikke vær 0.");
        }
    }
    public brok(int teller){
        this.teller = teller;
        this.nevner = 1;
    }

    public int getNevner() {
        return nevner;
    }

    public int getTeller() {
        return teller;
    }

    public void addBrok(brok brokPluss){
        if (brokPluss.nevner != this.nevner) {

            int nyTeller1 = this.nevner * brokPluss.teller;

            int nyNevner1 = this.nevner * brokPluss.nevner;
            int nyTeller2 = this.teller * brokPluss.nevner;

            int sumTeller = nyTeller2 + nyTeller1;

            this.nevner = nyNevner1;
            this.teller = sumTeller;

        } else {
            this.teller = brokPluss.teller + this.teller;
        }
    }

    public void subtractBrok(brok brokSubtract){
        if (brokSubtract.nevner != this.nevner) {

            int nyTeller1 = this.nevner * brokSubtract.teller;

            int nyNevner1 = this.nevner * brokSubtract.nevner;
            int nyTeller2 = this.teller * brokSubtract.nevner;

            int sumTeller = nyTeller2 - nyTeller1;

            this.nevner = nyNevner1;
            this.teller = sumTeller;

        } else {
            this.teller = this.teller - brokSubtract.teller ;
        }
    }

    public void multiplyBrok(brok brokMultiply){
        this.teller = brokMultiply.teller * this.teller;
        this.nevner = brokMultiply.nevner * this.nevner;
    }

    public void divdeBrok(brok brokDivide){
        int nyNevner = brokDivide.teller * this.nevner;
        this.teller = brokDivide.nevner * this.teller;
        this.nevner = nyNevner;
    }

}
