import java.util.Random;

public class MinRandom {
    private final Random randomGenerator;

    public MinRandom() {
        randomGenerator = new Random();
    }

    public int nesteHeltall(int nedre, int ovre){
        return randomGenerator.nextInt(ovre - nedre) + nedre;
    }

    public double nesteDesimalTall(double nedre, double ovre){
        int heltall = nesteHeltall((int) nedre, (int) ovre);
        double desimal = randomGenerator.nextDouble();
        return heltall + desimal;
    }
}
