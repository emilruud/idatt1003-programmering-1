import javax.lang.model.element.NestingKind;
import java.security.PrivateKey;
import java.util.StringTokenizer;

public class Arrangement {
    private final int nummer;
    private String navn;
    private String sted;
    private String arrangør;
    private String type;
    private int dato;
    private int tidpunkt;

    public Arrangement(int nummer, String navn, String sted, String arrangør, String type, int dato, int tidpunkt) {
        this.nummer = nummer;
        this.navn = navn;
        this.sted = sted;
        this.arrangør = arrangør;
        this.type = type;
        this.dato = dato;
        this.tidpunkt = tidpunkt;

    }

    public int getNummer() {
        return nummer;
    }

    public String getNavn() {
        return navn;
    }

    public String getSted() {
        return sted;
    }

    public String getArrangør() {
        return arrangør;
    }

    public String getType() {
        return type;
    }

    public long getDato() {
        return dato;
    }

    public long getTidpunkt(){
        return tidpunkt;
    }

    public String toString() {
        return "Dato: " +  + dato + "\n" +
                "Klokke: " + tidpunkt + "\n" +
                "Type: " + type + "\n" +
                "Navn: " + navn + "\n" +
                "Sted: " + sted + "\n" +
                "Arrangør: " + arrangør;
    }
}
