import java.sql.Array;
import java.util.ArrayList;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Arrangement arrangement1 = new Arrangement(1, "Konsert", "Konsertsalen", "Orkesteret", "Konsert", 20221030, 1800);
        Arrangement arrangement2 = new Arrangement(2, "Foredrag", "Kulturhuset", "Foredragsholderen", "Foredrag", 20221031, 1000);
        Arrangement arrangement3 = new Arrangement(3, "Teater", "Teatersalen", "Teatergruppen", "Teater", 20221030, 1200);
        Arrangement arrangement4 = new Arrangement(4, "Utstilling", "Galleri Kunstner", "Kunstforeningen", "Utstilling", 20221030, 1400);
        Arrangement arrangement5 = new Arrangement(5, "Musikal", "Teatersalen", "Teatergruppen", "Musikal", 20221110, 1900);
        Arrangement arrangement6 = new Arrangement(6, "musikal2", "Kulturhuset", "Musikgruppen", "Musikal", 20221115, 1600);

        ArrangementRegister register = new ArrangementRegister();
        register.registrerArrangemnet(arrangement1);
        register.registrerArrangemnet(arrangement2);
        register.registrerArrangemnet(arrangement3);
        register.registrerArrangemnet(arrangement4);
        register.registrerArrangemnet(arrangement5);
        register.registrerArrangemnet(arrangement6);


        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("\nMenu:");
            System.out.println("1. Register new arrangement");
            System.out.println("2. Find arrangements by location");
            System.out.println("3. Find arrangements by date");
            System.out.println("4. Find arrangements within date range");
            System.out.println("5. Display all arrangements sorted by location");
            System.out.println("6. Display all arrangements sorted by type");
            System.out.println("7. Display all arrangements sorted by date and time");
            System.out.println("8. Exit");
            System.out.print("Enter your choice: ");

            choice = scanner.nextInt();
            scanner.nextLine();  // Consume newline

            switch (choice) {
                case 1:
                    System.out.print("Enter arrangement number: ");
                    int nummer = scanner.nextInt();
                    scanner.nextLine();  // Consume newline

                    System.out.print("Enter arrangement name: ");
                    String navn = scanner.nextLine();

                    System.out.print("Enter location: ");
                    String sted = scanner.nextLine();

                    System.out.print("Enter organizer: ");
                    String arrangør = scanner.nextLine();

                    System.out.print("Enter type: ");
                    String type = scanner.nextLine();

                    System.out.print("Enter date (yyyymmdd): ");
                    int dato = scanner.nextInt();

                    System.out.print("Enter time (hhmm): ");
                    int tidspunkt = scanner.nextInt();

                    Arrangement arrangement = new Arrangement(nummer, navn, sted, arrangør, type, dato, tidspunkt);
                    register.registrerArrangemnet(arrangement);
                    System.out.println("Arrangement registered!");
                    break;
                case 2:
                    System.out.print("Enter location to search for: ");
                    String searchLocation = scanner.nextLine();
                    ArrayList<Arrangement> byLocation = register.arrangementerPåEtGittSted(searchLocation);
                    displayArrangements(byLocation);
                    break;
                case 3:
                    System.out.print("Enter date to search for (yyyymmdd): ");
                    int searchDate = scanner.nextInt();
                    ArrayList<Arrangement> byDate = register.arrangementerPåEtGittDato(searchDate);
                    displayArrangements(byDate);
                    break;
                case 4:
                    System.out.print("Enter start date of range (yyyymmdd): ");
                    int startDate = scanner.nextInt();

                    System.out.print("Enter end date of range (yyyymmdd): ");
                    int endDate = scanner.nextInt();

                    ArrayList<Arrangement> inRange = register.arrqangementerIntervall(startDate, endDate);
                    displayArrangements(inRange);
                    break;
                case 5:
                    ArrayList<Arrangement> byLocationSorted = register.sorterEtterSted();
                    displayArrangements(byLocationSorted);
                    break;
                case 6:
                    ArrayList<Arrangement> byTypeSorted = register.sorterEtterType();
                    displayArrangements(byTypeSorted);
                    break;
                case 7:
                    ArrayList<Arrangement> byDateTimeSorted = register.sorterEtterTidpunkt();
                    displayArrangements(byDateTimeSorted);
                    break;
                case 8:
                    System.out.println("Exiting program. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }

        } while (choice != 8);
    }

    public static void displayArrangements(ArrayList<Arrangement> arrangementer) {
        if (arrangementer.isEmpty()) {
            System.out.println("No arrangements found.");
            return;
        }

        for (Arrangement arrangement : arrangementer) {
            System.out.println(arrangement + "\n");
        }
    }
}