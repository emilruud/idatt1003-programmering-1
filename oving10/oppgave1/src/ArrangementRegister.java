import java.util.*;

public class ArrangementRegister {
    private ArrayList<Arrangement> ArrangementRegister = new ArrayList<>();

    public void registrerArrangemnet(Arrangement arrangement){
        ArrangementRegister.add(arrangement);
    }
    public ArrayList<Arrangement> getArrangementRegister(){
        return ArrangementRegister;
    }

    ArrayList<Arrangement> arrangementerPåEtGittSted(String sted){
        ArrayList<Arrangement> paaSted = new ArrayList<>();
        for (int i = 0; i < ArrangementRegister.size(); i++){
            if (ArrangementRegister.get(i).getSted().equals(sted)){
                paaSted.add(ArrangementRegister.get(i));
            }
        }
        return paaSted;
    }

    ArrayList<Arrangement> arrangementerPåEtGittDato(int dato){
        ArrayList<Arrangement> paaDato = new ArrayList<>();
        for (Arrangement arrangement : ArrangementRegister) {
            if (arrangement.getDato() == dato) {
                paaDato.add(arrangement);
            }
        }
        return paaDato;
    }

    ArrayList<Arrangement> arrqangementerIntervall(int dato1, int dato2){
        ArrayList<Arrangement> innenfor = new ArrayList<>();
        for (Arrangement arrangement : ArrangementRegister) {
            if (dato1 < arrangement.getDato() && arrangement.getDato() < dato2){
                innenfor.add(arrangement);
            }
        }
        innenfor.sort(Comparator.comparing(Arrangement::getDato).thenComparing(Arrangement::getTidpunkt));

        return innenfor;
    }

    ArrayList<Arrangement> sorterEtterTidpunkt(){
        ArrayList<Arrangement> sortert = getArrangementRegister();
        sortert.sort(Comparator.comparing(Arrangement::getDato).thenComparing(Arrangement::getTidpunkt));
        return sortert;
    }

    ArrayList<Arrangement> sorterEtterSted(){
        ArrayList<Arrangement> sortert = getArrangementRegister();
        sortert.sort(Comparator.comparing(Arrangement::getSted));
        return sortert;
    }

    ArrayList<Arrangement> sorterEtterType(){
        ArrayList<Arrangement> sortert = getArrangementRegister();
        sortert.sort(Comparator.comparing(Arrangement::getType));
        return sortert;
    }









}
