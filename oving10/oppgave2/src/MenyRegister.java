import java.util.ArrayList;
import java.util.List;

public class MenyRegister {
    private List<Meny> menyer;
    private List<Matrett> matretter;

    public MenyRegister(){
        this.menyer = new ArrayList<>();
        this.matretter = new ArrayList<>();
    }

    public void leggTilRett(Matrett matrett){
        matretter.add(matrett);
    }

    public List<Meny> getMenyRegister(){
        return menyer;
    }
    public Matrett finnMatrettGittNavn(String navn){
        for (Matrett matrett : matretter){
            if (matrett.getNavn().equals(navn)){
                return matrett;
            }
        }
        return null;
    }

    public List<Matrett> finnMatrettGittType(String type){
        List<Matrett> resultat = new ArrayList<>();
        for (Matrett matrett : matretter){
            if (matrett.getType().equals(type)){
                resultat.add(matrett);
            }
        }
        return resultat;
    }

    public void leggTilMeny(Meny meny){
        menyer.add(meny);
    }

    public List<Meny> finnMenyerInnenforPrisintervall(double minPris, double maxPris){
        List<Meny> resultat = new ArrayList<>();
        for (Meny meny : menyer){
            if (meny.getTotalPris() >= minPris && meny.getTotalPris() <= maxPris){
                resultat.add(meny);
            }
        }
        return resultat;
    }
}
