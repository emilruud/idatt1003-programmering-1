import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MenyRegister menyRegister = new MenyRegister();

        Matrett forrett1 = new Matrett("Forrett 1", "Forrett", 50.0, "Oppskrift for forrett 1");
        Matrett hovedrett1 = new Matrett("Hovedrett 1", "Hovedrett", 100.0, "Oppskrift for hovedrett 1");
        Matrett hovedrett2 = new Matrett("Hovedrett 2", "Hovedrett", 200.0, "Oppskrift for hovedrett 2");
        Matrett dessert1 = new Matrett("Dessert 1", "Dessert", 30.0, "Oppskrift for dessert 1");

        Meny meny1 = new Meny();
        meny1.leggTilRett(forrett1);
        meny1.leggTilRett(hovedrett1);

        Meny meny2 = new Meny();
        meny2.leggTilRett(hovedrett1);
        meny2.leggTilRett(dessert1);

        Meny meny3 = new Meny();
        meny3.leggTilRett(forrett1);
        meny3.leggTilRett(hovedrett1);
        meny3.leggTilRett(dessert1);

        menyRegister.leggTilRett(forrett1);
        menyRegister.leggTilRett(hovedrett1);
        menyRegister.leggTilRett(dessert1);
        menyRegister.leggTilRett(hovedrett2);

        menyRegister.leggTilMeny(meny1);
        menyRegister.leggTilMeny(meny2);
        menyRegister.leggTilMeny(meny3);


        List<Meny> result = menyRegister.finnMenyerInnenforPrisintervall(100.0, 200.0);

        for (Meny meny : result) {
            System.out.println("Totalpris: " + meny.getTotalPris());
            for (Matrett rett : meny.getMatretter()) {
                System.out.println(rett.getNavn() + " - " + rett.getType() + " - " + rett.getPris());

            }
            System.out.println();
        }
        System.out.println();

        Matrett forrett = menyRegister.finnMatrettGittNavn("Forrett 1");
        System.out.println(forrett.getNavn());

        List<Matrett> hovedretter = new ArrayList<>();
        hovedretter = menyRegister.finnMatrettGittType("Hovedrett");

        System.out.println();

        System.out.println("Hovedretter: ");
        for (Matrett matrett : hovedretter) {
            System.out.println(matrett.getNavn());
        } 




    }
}