import java.util.ArrayList;
import java.util.List;

public class Meny {
    private List<Matrett> matretter;

    public Meny(){
        this.matretter = new ArrayList<>();
    }
    public void leggTilRett(Matrett matrett){
        matretter.add(matrett);
    }

    public List<Matrett> getMatretter(){
        return matretter;
    }

    public double getTotalPris(){
        double totalPris = 0;
        for (Matrett matrett : matretter){
            totalPris += matrett.getPris();
        }
        return totalPris;
    }
}
