import java.util.Random;
public class tilfeldigtallgenerator {

    public void printUtAntallGangerEtTalloppstaar(int iterasjoner){
        Random tallgenerator = new Random();
        int [] fordelingAvTall = new int[10];
        for (int i = 0; i < iterasjoner; i++){
            int nyttTall = tallgenerator.nextInt(10);
            fordelingAvTall[nyttTall]++;
        }
        System.out.println("Det ble generert " + iterasjoner + " tall mellom 1 og 10.");
        for (int i = 0; i < fordelingAvTall.length; i++){
            System.out.println(i + ": " + fordelingAvTall[i]);
        }
    }
}

