import javax.swing.*;
import java.util.Scanner;

public class tekstanalyse {

    int[] antallTegn;

    public tekstanalyse(String tekst) {
        antallTegn = new int[30];

        for (int i = 0; i < tekst.length(); i++){
            char tegn = tekst.charAt(i);
            if (tegn == 230){
                antallTegn[26]++;
            } else if (tegn == 248) {
                antallTegn[27]++;
            } else if (tegn == 229) {
                antallTegn[28]++;
            } else if (Character.isLetter(tegn)){
                    int index = Character.toLowerCase(tegn)- 'a'; //trekker fra a for å få det imellom 0-30;
                    antallTegn[index]++; // plusser på en på index for å telle tegnet.
            } else{
                antallTegn[antallTegn.length-1]++;
            }
        }
    }
    public int antallForskjelligeBokstaver(){
        int forskjelligeBokstaver = 0;
        for (int i = 0; i < antallTegn.length-1; i++){
            if (antallTegn[i] != 0){
                forskjelligeBokstaver++;
            }
        }
        return forskjelligeBokstaver;
    }

    public int totAntBokstaver(){
        int sum = 0;
        for(int i = 0; i < antallTegn.length-1; i++){
            sum += antallTegn[i];
        }
        return sum;
    }

    public double prosentAndelBokstaver(){
        double totAntallBokstaver = 0;
        for(int i = 0; i < antallTegn.length-1; i++){
            totAntallBokstaver += antallTegn[i];
        }
        return totAntallBokstaver/(totAntallBokstaver+antallTegn[29]) * 100;
    }

    public int totAntAvEnBokstav(Character tegn){
        if (tegn == 230){
            return antallTegn[26];
        } else if (tegn == 248) {
            return antallTegn[27];
        } else if (tegn == 229) {
            return antallTegn[28];
        } else if (Character.isLetter(tegn)){
            tegn = Character.toLowerCase(tegn);
            return antallTegn[tegn - 'a'];
        } else{
            return antallTegn[29];
        }
    }

    public void bokstavDetErMestAv(){
        int hoyeste = 0;
        Character[] mestAv = new Character[29];
        for (int i = 0; i < antallTegn.length-1; i++){
            if (antallTegn[i] > hoyeste){
                hoyeste = antallTegn[i];
                mestAv = new Character[29]; //nullstille lista
                mestAv[0] = (char) (i + 97);
            } else if (antallTegn[i] == hoyeste) {
                mestAv[i] = (char) (i + 97);
            }
        }
        for (int i = 0; i < antallTegn.length - 1; i++) {
            if (mestAv[i] != null)
                System.out.print(mestAv[i] + " ");
        }
    }

}
