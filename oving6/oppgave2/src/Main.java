import javax.swing.JOptionPane;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        while (true) {
            System.out.println("Skeiv inn teksten du vil analysere ");
            String text = scanner.nextLine();
            if (text.isEmpty()){
                break;
            }
            tekstanalyse tekstanalyse1 = new tekstanalyse(text);
            System.out.println("Antall forskjellige bokstaver er: " + tekstanalyse1.antallForskjelligeBokstaver());
            System.out.println("Totalt antall bokstaver er: " + tekstanalyse1.totAntBokstaver());
            System.out.println("Prosentandelen bokstaver i teksten er: " + tekstanalyse1.prosentAndelBokstaver());
            System.out.println("Skriv inn en bokstav du vil finne ut hvor mange det er av: ");
            String bokstavString = scanner.nextLine();
            char bokstavChar = bokstavString.charAt(0);
            System.out.println("Antall ganger bokstaven " + bokstavString + " er i teksten: " + tekstanalyse1.totAntAvEnBokstav(bokstavChar));
            System.out.println("Det er mest av bokstaven: ");
            tekstanalyse1.bokstavDetErMestAv();
            System.out.println();

            //System.out.println(tekstanalyse1.totAntAvEnBokstav('e'));

        }
    }
}