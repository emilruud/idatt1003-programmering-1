public class matrix {
    private final int[][] matrix;

    public matrix(int[][] matrix1) {
        int rows = matrix1.length;
        int colums = matrix1[0].length;
        this.matrix = new int[rows][colums];

        for (int i = 0; i < rows; i++){
            System.arraycopy(matrix1[i], 0, this.matrix[i], 0, colums);
        }
    }
    public int[][] getMatrix() {
        return matrix;
    }

    public matrix addMatrix1(matrix matrix1){
        int[][] sumMatrix = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                sumMatrix[i][j] = matrix1.matrix[i][j] + matrix[i][j];
            }
        }
        return new matrix(sumMatrix);
    }

    public matrix multiplyMatrix1(matrix matrix1){
        int[][] result = new int[this.matrix.length][matrix1.matrix[0].length];

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < matrix1.matrix[0].length; j++) {
                for (int k = 0; k < this.matrix[0].length; k++) {
                    result[i][j] += this.matrix[i][k] * matrix1.matrix[k][j];
                }
            }
        }

        return new matrix(result);
    }

    public matrix transpose(){
        int [][] transposeMatrix = new int[this.matrix.length][this.matrix[0].length];

        for (int i = 0; i < this.matrix.length; i++){
            for (int j = 0; j < this.matrix.length; j++){
                transposeMatrix[i][j] =this.matrix[j][i];
            }
        }
        return new matrix(transposeMatrix);
    }


    public void printMatrix(){
        for (int[] ints : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
    }
}
