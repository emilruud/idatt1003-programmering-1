// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[][] inputMatrix1 = {{1,2,3}, {4,5,6}, {7,8,9}};
        matrix matrix1 = new matrix (inputMatrix1);

        int[][] inputMatrix2 = {{1,0,0}, {0,1,0}, {0,0,1}};
        matrix matrix2 = new matrix(inputMatrix2);

        matrix sumMatrix = matrix1.addMatrix1(matrix2);

        matrix multMatrix = matrix1.multiplyMatrix1(matrix2);

        matrix transposeMatrix = matrix1.transpose();
        System.out.println("Addere matrise:");
        sumMatrix.printMatrix();
        System.out.println("multiplisere matrise:");
        multMatrix.printMatrix();
        System.out.println("Transponere matrise:");
        transposeMatrix.printMatrix();
    }
}