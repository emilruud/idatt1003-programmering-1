package ovinger;
import static javax.swing.JOptionPane.*;
import java.util.Scanner;

class oving2_oppgave1 {
    public static void main(String[] args){

        String AarstallString = showInputDialog("Årstall: ");
        int Aarstall = Integer.parseInt(AarstallString);
        if (Aarstall % 4 == 0){
            if ((Aarstall % 100 != 0) || (Aarstall % 400 == 0)){
                    System.out.println(Aarstall + " er et skuddår");
            } else{
                System.out.println(Aarstall + " er ikke et skuddår");}
        } else {
            System.out.println(Aarstall + " er ikke et skuddår");
        }
    }
}

class oving2_oppgave2 {
    public static void main(String[] args){
            
        Scanner scanner = new Scanner(System.in);

        System.out.print("Skriv inn pris på kjøttdeig nr1: ");
        float kjottdeig1Pris = scanner.nextFloat();
        System.out.print("Skriv inn antall gram på kjøttdeig nr1: ");
        float kjottdeig1Gram = scanner.nextFloat();

        System.out.print("Skriv inn pris på kjøttdeig nr2: ");
        float kjottdeig2Pris = scanner.nextFloat();
        System.out.print("Skriv inn antall gram på kjøttdeig nr2: ");
        float kjottdeig2Gram = scanner.nextFloat();

        float prisPerKG1 = kjottdeig1Pris/kjottdeig1Gram;
        float prisPerKG2 = kjottdeig2Pris/kjottdeig2Gram;

        if (prisPerKG1 < prisPerKG2){
            System.out.println("kjøttdeig1 er billigst");
        } else{
            System.out.println("kjøttdeig2 er billigst");
        }

        scanner.close();
    }
}