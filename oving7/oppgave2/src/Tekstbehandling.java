public class Tekstbehandling {
    private final String str;

    public Tekstbehandling(String str) {
        this.str = str;
    }

    public int antallOrd(){
        String[] ord = str.split(" ");
        return ord.length;
    }

    public double snittLengdeOrd(){
        String[] ord = str.split(" ");
        int teller = 0;
        for(String s : ord){
            teller += s.length();
        }
        return (double) teller /ord.length;
    }

    public double snittLengdePerSetning(){
        String[] setning = str.split("[.,!?]+");
        int teller = 0;
        for (String s : setning){
            String nyS = s.strip();
            String[] ordISetning = nyS.split(" ");
            int antallOrd = ordISetning.length;
            teller += antallOrd;
        }
        return (double) teller / setning.length;
    }

    public String skifteUtOrd(String gammelOrd, String nyttOrd){
        return str.replace(gammelOrd, nyttOrd);
    }

    public String getString(){
        return str;
    }

    public String getStringCaps(){
        return str.toUpperCase();
    }




    

}
