// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Tekstbehandling test = new Tekstbehandling("kuk.");
        System.out.println(test.antallOrd());
        System.out.println(test.snittLengdeOrd());
        System.out.println(test.snittLengdePerSetning());
        System.out.println(test.skifteUtOrd("kuk", "bla"));
        System.out.println(test.getString());
        System.out.println(test.getStringCaps());
    }
}