// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        NyString test = new NyString("denne setningen kan forkortes æøåå");
        System.out.println(test.forkorting());
        System.out.println(test.fjernTegn("e"));
    }
}