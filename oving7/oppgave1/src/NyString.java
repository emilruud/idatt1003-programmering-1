import java.util.Arrays;

public class NyString {
    private final String str;

    public NyString(String str){
        this.str = str;
    }
    public String forkorting(){
        String[] ord = str.split(" ");
        String resultat = "";
        for (String s : ord){
            resultat += s.charAt(0);
        }
        return resultat;
    }

    public String fjernTegn(String bokstav){
        return str.replace(bokstav, "");
    }




}
