import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        valuta euro = new valuta("Euro", 11.5);
        valuta dollar = new valuta("Dollar", 10.71);
        valuta svenskeKroner = new valuta("Svenske kroner", 0.96);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Do you want to convert:");
        System.out.println("1. To NOK");
        System.out.println("2. From NOK");
        int conversion = scanner.nextInt();
        while(true) {
            switch (conversion) {
                case (1):
                    while (true) {
                        System.out.println("Menu:");
                        System.out.println("1. Convert from EURO to NOK");
                        System.out.println("2. Convert from Dollar to NOK");
                        System.out.println("3. Convert from SEK to NOK");
                        System.out.println("4. Exit");

                        System.out.println("Enter your choice:");
                        int choice = scanner.nextInt();
                        if (choice >= 1 && choice <= 3) {
                            System.out.println("How much do you want to convert to NOK");
                            int value = scanner.nextInt();

                            switch (choice) {
                                case 1 -> System.out.println(value + " EURO converted to NOK is " + euro.tilNorske(value));
                                case 2 -> System.out.println(value + " Dollar converted to NOK is " + dollar.tilNorske(value));
                                case 3 -> System.out.println(value + " SEK converted to NOK is " + svenskeKroner.tilNorske(value));
                            }
                        } else if (choice == 4) {
                            System.out.println("Goodbye");
                            scanner.close();
                            System.exit(0);
                        } else {
                            System.out.println("Invalid choice. Please try again.");
                        }
                    }
                case (2):
                    while (true) {
                        System.out.println("Menu:");
                        System.out.println("1. Convert from NOK to EURO");
                        System.out.println("2. Convert from NOK to Dollar");
                        System.out.println("3. Convert from NOK to SEK");
                        System.out.println("4. Exit");

                        System.out.println("Enter your choice:");
                        int choice = scanner.nextInt();
                        if (choice >= 1 && choice <= 3) {
                            System.out.println("How much do you want to convert");
                            int value = scanner.nextInt();

                            switch (choice) {
                                case 1 -> System.out.println(value + " NOK converted to EURO is " + euro.fraNorske(value));
                                case 2 -> System.out.println(value + " NOK converted to Dollar is " + dollar.fraNorske(value));
                                case 3 -> System.out.println(value + " NOK converted to SEK is " + svenskeKroner.fraNorske(value));
                            }
                        } else if (choice == 4) {
                            System.out.println("Goodbye");
                            scanner.close();
                            System.exit(0);
                        } else {
                            System.out.println("Invalid choice. Please try again.");
                        }
                    }
            }
        }
    }
}
