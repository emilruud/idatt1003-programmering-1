import java.util.Random;

public class Player {
    private int sumPoeng;

    public Player() {
        this.sumPoeng = 0;
    }

    public int getSumPoeng(){
        return sumPoeng;
    }
    public void kastTerningen(){
        Random terning = new Random();
        int resultat = terning.nextInt(6) + 1;
        if (resultat == 1){
            sumPoeng = 0;
        } else if (sumPoeng > 100) {
            sumPoeng -= resultat;
        } else {
            sumPoeng += resultat;
        }
    }
    public boolean erFerdig(){
        return sumPoeng == 100;
    }
}