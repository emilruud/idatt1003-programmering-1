// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Player A = new Player();
        Player B = new Player();
        int antallKast = 0;

        while(!(A.erFerdig() || B.erFerdig())){
            A.kastTerningen();
            B.kastTerningen();

            System.out.println("A sin score er " + A.getSumPoeng());
            System.out.println("B sin score er " + B.getSumPoeng());
            antallKast++;
            System.out.println();
        }
        if (A.erFerdig()) {
            System.out.println(" A vant terningspillet");
            System.out.println(" A brukte " + antallKast + " antall kast");
        }
        else {
            System.out.println("B vant terningspillet");
            System.out.println(" B brukte " + antallKast + " antall kast");
        }
    }
}
