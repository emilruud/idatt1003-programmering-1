import java.lang.module.FindException;

public class OppgaveOversikt {
    private Student[] studenter;
    private int antStud = 0;

    OppgaveOversikt(Student[] studenter){
        this.studenter = studenter;
        antStud = studenter.length;
    }

    public int getAntStud() {
        return antStud;
    }

    public int antOppgEnStudent(Student student){
        int i = 0;
        while (i < studenter.length){
            if (studenter[i] == student){
                return studenter[i].getAntOppgave();
            }
            i++;
        }
        return -1;
    }


    public void addStudent(Student student){
        Student[] nyStudenter = new Student[studenter.length+1];
        for (int i = 0; i < studenter.length; i++){
            nyStudenter[i] = studenter[i];
        }
        nyStudenter[nyStudenter.length-1] = student;
        antStud++;

        studenter = nyStudenter;
    }


    public void leggeTilOppgaverTilEnStudent(Student student, int leggeTil){
        int index = 0;
        for(int i = 0; i < studenter.length; i++){
            if (studenter[i] == student){
                index = i;
                break;
            }
        }
        studenter[index].okAntOppg(leggeTil);
    }

    public String toString(){
        String resultat = "";
        for (int i = 0; i < studenter.length; i++){
            resultat += studenter[i].toString();
            resultat += "\n";
        }
        return resultat;
    }
}
