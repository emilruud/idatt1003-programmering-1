// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
       Student Emil = new Student("Emil Ruud", 2);
       Student Johan = new Student("Joahn", 1);
       Student Sverre = new Student("Sverre", 3);
       Student Niclas = new Student("Nic", 1);
       Student[] klasse = {Emil, Johan};

       OppgaveOversikt oversikt = new OppgaveOversikt(klasse);
       oversikt.addStudent(Sverre);
       oversikt.addStudent(Niclas);
       oversikt.leggeTilOppgaverTilEnStudent(Emil, 4);
       oversikt.leggeTilOppgaverTilEnStudent(Sverre, (2));
       System.out.println(oversikt);
    }
}