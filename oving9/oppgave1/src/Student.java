public class Student {
    private String navn;
    private int antOppgave;

    public Student(String navn, int antOppgave) {
        this.navn = navn;
        this.antOppgave = antOppgave;
    }

    public int getAntOppgave() {
        return antOppgave;
    }
    public String getNavn(){
        return navn;
    }

    public void okAntOppg(int okning){
        antOppgave += okning;
    }

    public String toString(){
        return navn + " har fullført " + antOppgave;
    }

}