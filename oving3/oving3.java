package ovinger;
import java.util.Scanner;

class oving3oppgave1 {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Skriv inn hvilken del av gangetabellen du vil starte med:");
        int forsteDel = scanner.nextInt(); 
        System.out.print("Skriv inn hvilken gangetabell du vil slutte med:");
        int sisteDel = scanner.nextInt();

        while (forsteDel <= sisteDel ){
            System.out.println(forsteDel + "-gangen:");
            for (int i = 1; i <= 10; i++){
                System.out.println(forsteDel + " * " + i + " = " + forsteDel * i);
                
            }
            System.out.println();
            forsteDel++;
        }

        scanner.close();
    }
    
}

class oving3oppgave2 {
    public static void main(String[] args){

    Scanner scanner = new Scanner(System.in);
    String fortsett;
    
    do{
        System.out.println("Skriv inn et tall som du vil sjekk om er primtall:");
        int tall = scanner.nextInt();
        int dele = tall-1;
        boolean isPrime = true;

        if (tall < 0){
            System.out.println(tall + "negative tall kan ikke være primtall");
        } else if(tall == 0){
            System.out.println("skriv inn et positivt heltall");
        } else if(tall == 1){
          System.out.println("1 er ikke et primtall");  
        } else{
             while (dele > 1){
                    if(tall % dele == 0){
                        isPrime = false;
                        break;
                    }
                    dele--;
                }
            if(isPrime){//teller == 0){
                System.out.println(tall + " er et primtall");
            } else{
                System.out.println(tall + " er ikke et primtall");
            }   
        }
        
        System.out.println("Vil du fortsette? Skriv 'ja' for å fortsette, eller trykk enter for å avslutte.");
        scanner.nextLine(); // Konsumerer linjeskiftet
        fortsett = scanner.nextLine();

        } while (fortsett.equals("ja"));

        scanner.close();
    }
}