package ovinger;
import static javax.swing.JOptionPane.*;

class oppgave1 {
    public static void main(String[] args) {
        String tommerLest = showInputDialog("Lengde i tommer: ");
        double tommer = Double.parseDouble(tommerLest);
        tommer *= 2.54;
        System.out.println(tommer);
    }
}

class oppgave2{
    public static void main(String[] args) {
        String timerLest = showInputDialog("Skriv inn antall timer:");
        String minutterLest = showInputDialog("Skriv inn antall mintter:");
        String sekunderLest = showInputDialog("Skriv inn antall sekunder:");
        int timer = Integer.parseInt(timerLest);
        int minutter = Integer.parseInt(minutterLest);
        int sekunder = Integer.parseInt(sekunderLest);
        int sum = timer * 3600 + minutter * 60 + sekunder;
        System.out.println(sum);
    }  

}

class oppgave3{
    public static void main(String[] args) {
        String sekunderLest = showInputDialog("Skriv inn antall sekunder:");
        int sekunder = Integer.parseInt(sekunderLest);
        int antallTimer = sekunder / 3600;
        sekunder -= antallTimer * 3600;
        int antallMinutt = sekunder / 60;
        sekunder -= antallMinutt * 60;

        System.out.println(antallTimer + " timer");
        System.out.println(antallMinutt + " minutt");
        System.out.println(sekunder + " sekunder");
    }
}