import java.util.Scanner;  // Import the Scanner class
public class Main {
    public static void main(String[] args) {
        Person Emil = new Person("Emil", "Ruud", 2003);
        ArbTaker EmilArbtaker = new ArbTaker(Emil, 1, 2022, 100000, 20);
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;

        while (loop) {
            System.out.println("Meny:");
            System.out.println("1. Print ut info om arbeidstaker");
            System.out.println("2. Endre arbeidstakerens nummer");
            System.out.println("3. Endre ansettelsesår");
            System.out.println("4. Endre månedslønn");
            System.out.println("5. Endre skatteprosent");
            System.out.println("6. avslutt");
            int valg = scanner.nextInt();


            switch (valg) {
                case 1:
                    System.out.println(EmilArbtaker.fulltNavn());
                    System.out.println("Arbeidstaker nummer: " + EmilArbtaker.getArbetakernr());
                    System.out.println("Alder: " + EmilArbtaker.alder());
                    System.out.println("Antall år ansatt: " + EmilArbtaker.antallÅrAnsatt());
                    System.out.println("Månedslønn: " + EmilArbtaker.getMånedslønn());
                    System.out.println("Skatteprosent: " + EmilArbtaker.getSkatteprosent());
                    System.out.println("Skattetrekk hver måned: " + EmilArbtaker.kronerTrukketHverMåned());
                    System.out.println("Skatte trekk hvert år: " + EmilArbtaker.skatteTrekkeHvertÅr());
                    System.out.println("Bruttolønn: " + EmilArbtaker.bruttoLønn());
                    System.out.println();
                    break;
                case 2:
                    System.out.println("Hva vil du endre arbeidstaker nummert til?");
                    int nyttArbNummer = scanner.nextInt();
                    EmilArbtaker.setArbetakernr(nyttArbNummer);
                    System.out.println("Nytt arbeidtakernummer er: " + EmilArbtaker.getArbetakernr());
                    System.out.println();
                    break;

                case 3:
                    System.out.println("Hva vil du ansettelsesåret til? ");
                    int nyttAnsettelsesÅr = scanner.nextInt();
                    EmilArbtaker.setAnsettelsesår(nyttAnsettelsesÅr);
                    System.out.println("Nytt ansettelsesår er: " + EmilArbtaker.getAnsettelsesår());
                    System.out.println();
                    break;

                case 4:
                    System.out.println("Hva vil du endre månedslønn til?");
                    int nyMånedslønn = scanner.nextInt();
                    EmilArbtaker.setMånedslønn(nyMånedslønn);
                    System.out.println("Ny månedslønn er: " + EmilArbtaker.getMånedslønn());
                    System.out.println();
                    break;

                case 5:
                    System.out.println("Hva vil du endre skatteprosent til?");
                    int nySkatteprosent = scanner.nextInt();
                    EmilArbtaker.setSkatteprosent(nySkatteprosent);
                    System.out.println("Ny skatteprosent: " + EmilArbtaker.getSkatteprosent());
                    System.out.println("Skattetrekk hver måned: " + EmilArbtaker.kronerTrukketHverMåned());
                    System.out.println("Skatte trekk hvert år: " + EmilArbtaker.skatteTrekkeHvertÅr());
                    System.out.println("Bruttolønn: " + EmilArbtaker.bruttoLønn());
                    System.out.println();
                    break;
                case 6:
                    loop = false;
                    break;
            }
        }
    }
}

