
public class ArbTaker {
    private final Person personalia;
    private int arbetakernr;
    private int ansettelsesår;
    private int månedslønn;
    private int skatteprosent;


    public ArbTaker(Person personalia, int arbetakernr, int ansettelsesår, int månedslønn, int skatteprosent) {
        this.personalia = personalia;
        this.arbetakernr = arbetakernr;
        this.ansettelsesår = ansettelsesår;
        this.månedslønn = månedslønn;
        this.skatteprosent = skatteprosent;
    }


    // Getters
    public Person getPersonalia() {
        return personalia;
    }

    public int getArbetakernr() {
        return arbetakernr;
    }

    public int getAnsettelsesår() {
        return ansettelsesår;
    }

    public int getMånedslønn() {
        return månedslønn;
    }

    public int getSkatteprosent() {
        return skatteprosent;
    }

    // Setters
    public void setArbetakernr(int arbetakernr) {
        this.arbetakernr = arbetakernr;
    }

    public void setAnsettelsesår(int ansettelsesår) {
        this.ansettelsesår = ansettelsesår;
    }

    public void setMånedslønn(int månedslønn) {
        this.månedslønn = månedslønn;
    }

    public void setSkatteprosent(int skatteprosent) {
        this.skatteprosent = skatteprosent;
    }


    public int kronerTrukketHverMåned(){
        return månedslønn * skatteprosent/100;
    }

    public int innværendeÅr(){
        java.util.GregorianCalendar kalender = new java.util.GregorianCalendar();
        return kalender.get(java.util.Calendar.YEAR);
    }

    public int bruttoLønn(){
        return månedslønn * 12;
    }

    public double skatteTrekkeHvertÅr(){
        return kronerTrukketHverMåned() * 10.5;
    }

    public String fulltNavn(){
        return getPersonalia().getEtternavn() + ", " + getPersonalia().getFornavn();
    }

    public int alder(){
        return innværendeÅr() - getPersonalia().getfødselsår();
    }

    public int antallÅrAnsatt(){
        return innværendeÅr() - getAnsettelsesår();
    }

    public boolean ansattMerEnnXAntallår(int årstall){
        return årstall < antallÅrAnsatt();
    }
}
